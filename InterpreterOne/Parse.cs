﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpreterOne
{
    class Parse
    {
        
        IDictionary<String, int> PRECEDENCE = new Dictionary<string, int>();
        
        public Parse(TokenStream input)
        {
            PRECEDENCE.Add("=", 1);

            PRECEDENCE.Add("||", 2);

            PRECEDENCE.Add("&&", 3);

            PRECEDENCE.Add("<", 7);
            PRECEDENCE.Add(">", 7);
            PRECEDENCE.Add("<=", 7);
            PRECEDENCE.Add(">=", 7);
            PRECEDENCE.Add("==", 7);
            PRECEDENCE.Add("!=", 7);

            PRECEDENCE.Add("+", 10);
            PRECEDENCE.Add("-", 10);

            PRECEDENCE.Add("*", 20);
            PRECEDENCE.Add("/", 20);
            PRECEDENCE.Add("%", 20);

        }
    }
}
