﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace InterpreterOne
{
    enum TokenTypes
    {
        PUNC,
        NUM,
        STR,
        KW,
        VAR,
        OP
    }
    class Token
    {
        private TokenTypes type;
        public TokenTypes Type
        {
            get
            {
                return type;
            }
            set
            {
                this.type = Type;
            }
        }
        private Object value;
        public Object Value
        {
            get
            {
                return value;
            }

            set
            {
                this.value = Value;
            }
        }
        public Token(TokenTypes type, Object value)
        {
            this.type = type;
            this.value = value;
        }
    }
    class TokenStream
    {
        Token current;
        InputStream input;
        InputStream Input
        {
            get;
        }
        String[] keywords = { "if", "then", "else", "lambda", "λ", "true", "false" };
        private IdentitiyReader identityReader;
        private OperatorReader operatorReader;
        private PunctuationReader punctuationReader;
        private NumberReader numberReader;
        private StringReader stringReader;
        private WhiteSpaceReader whiteSpaceReader;
        private CommentReader commentReader;

        public TokenStream(InputStream input)
        {
            this.input = input;
            identityReader          = new IdentitiyReader(this, input);
            operatorReader          = new OperatorReader(this, input);
            punctuationReader       = new PunctuationReader(this, input);
            numberReader            = new NumberReader(this, input);
            stringReader            = new StringReader(this, input);
            whiteSpaceReader        = new WhiteSpaceReader(this, input);
            commentReader           = new CommentReader(this, input);

        }

        public Token ReadNext()
        {
            whiteSpaceReader.ReadWhile();
            if ( input.EOF())
            {
                return null;
            }
            char ch = input.Peek();
            if (ch.Equals('#'))
            {
                commentReader.Read();
                return ReadNext();
            }
            if (stringReader.IsStart(ch))
            {
                return stringReader.Read();
            }
            if (numberReader.IsStart(ch))
            {
                return numberReader.Read();
            }
            if ( identityReader.IsStart(ch) )
            {
                return identityReader.Read();
            }
            if ( punctuationReader.IsStart(ch) )
            {
                punctuationReader.Read();
            }
            if ( operatorReader.IsStart(ch) )
            {
                return operatorReader.Read();
            }

            input.croak("Can't handel character: " + ch);

            return null;
        }
        public Token Peek()
        {

            return (current != null) ?  current : ReadNext();
        }
        public Token next()
        {
            Token tok = current;
            current = null;
            return (tok != null) ? tok : ReadNext();
        }
        public Boolean EOF()
        {
            return Peek() != null;
        }
    }

    
}
