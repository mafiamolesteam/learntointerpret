﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace InterpreterOne
{
    interface ITypeReader
    {
        Boolean IsStart(char value);
        Boolean Is(char value);
        Token Read();
    }
    abstract class ATypeReader
    {
        protected TokenStream stream;
        protected InputStream input;

        public ATypeReader(TokenStream str, InputStream input)
        {
            this.stream = str;
            this.input = input;
        }
        public String ReadWhile()
        {
            String str = "";
            while ( !input.EOF() && Is(input.Peek()) )
            {
                str += input.Next();
            }
            return str;
        }
        public abstract bool Is(char value);
    }
    class IdentitiyReader : ATypeReader, ITypeReader
    {
        String[] keywords = { "if", "then", "else", "lambda", "λ", "true", "false" };
        public IdentitiyReader(TokenStream str, InputStream input) : base(str, input)
        {
        }

        public override bool Is(char value)
        {
            return IsStart(value) || "?!-<>=0123456789".Contains(value);
        }

        public Token Read()
        {
            String id = ReadWhile();
            TokenTypes type = (IsKeyword(id) ? TokenTypes.KW : TokenTypes.VAR);
            return new Token(type, id);
        }
        public bool IsStart(char value)
        {
            Regex regex = new Regex("/a-zλ_/i");
            return regex.IsMatch(value.ToString());
        }
        public bool IsKeyword(string value)
        {
            return keywords.Contains(value);
        }
    }
    class OperatorReader : ATypeReader, ITypeReader
    {
        public OperatorReader(TokenStream str, InputStream input) : base(str, input)
        {
        }
        public override bool Is(char value)
        {
            return "+-*/%=&|<>!".Contains(value);
        }

        public Token Read()
        {
            String op = ReadWhile();
            return new Token(TokenTypes.OP,op);
        }
        public bool IsStart(char value)
        {
            throw new NotImplementedException();
        }
    }
    class PunctuationReader : ATypeReader, ITypeReader
    {
        public PunctuationReader(TokenStream str, InputStream input) : base(str, input)
        {
        }

        public override bool Is(char value)
        {
            return ",;(){}[]".Contains(value);
        }

        public Token Read()
        {
            return new Token(TokenTypes.PUNC, input.Next());
        }

        public bool IsStart(char value)
        {
            throw new NotImplementedException();
        }
    }
    class NumberReader : ATypeReader, ITypeReader
    {
        public NumberReader(TokenStream str, InputStream input) : base(str, input)
        {
        }

        public override bool Is(char value)
        {
            return IsStart(value) || value.Equals('.');
        }

        public bool IsStart(char value)
        {
            int result;
            return Int32.TryParse(value.ToString(), out result);
        }

        public Token Read()
        {
            String number = ReadWhile();
            return new Token(TokenTypes.NUM, float.Parse(number));
        }
    }
    class StringReader : ATypeReader, ITypeReader
    {
        public StringReader(TokenStream str, InputStream input) : base(str, input)
        {
        }

        public override bool Is(char value)
        {
            return IsStart(value);
        }

        public bool IsStart(char value)
        {
            return value.Equals('"');
        }

        public Token Read()
        {
            return new Token(TokenTypes.STR, ReadEscaped(""));
        }
        public String ReadEscaped(String end)
        {
            Boolean escaped = false;
            String str = "";
            input.Next();
            while(!input.EOF())
            {
                char ch = input.Next();
                if ( escaped )
                {
                    str += ch;
                    escaped = false;
                } else if ( ch.Equals('\\'))
                {
                    escaped = true;
                }
                else if ( ch.Equals(end) )
                {
                    break;
                } else
                {
                    str += ch;
                }
            }
            return str;
        }
    }
    class WhiteSpaceReader : ATypeReader, ITypeReader
    {
        public WhiteSpaceReader(TokenStream str, InputStream input) : base(str, input)
        {
        }

        public override bool Is(char value)
        {
            return " \t\n".Contains(value);
        }
        public Token Read()
        {
            ReadWhile();
            return null;
        }
        public bool IsStart(char value)
        {
            return " \t\n".Contains(value);
        }
    }
    class CommentReader : ATypeReader, ITypeReader
    {
        public CommentReader(TokenStream str, InputStream input) : base(str, input)
        {
        }

        public override bool Is(char value)
        {
            return value != '\n';
        }
        public Token Read()
        {
            ReadWhile();
            return null;
        }
        public bool IsStart(char value)
        {
            return "#".Contains(value);
        }
    }
}
