﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpreterOne
{
    class InputStream
    {
        int position    = 0;
        int line        = 1;
        int column      = 0;
        String input;

        public InputStream(String input)
        {
        }

        public char Next()
        {
            char character = input[position++];
            if (character.Equals("\n"))
            {
                line++;
                column = 0;
            } else
            {
                column++;
            }
            return character;
        }
        public char Peek()
        {
            return input[position];
        }
        public bool EOF()
        {
            return position >= input.Length;
        }
        public void croak(String message)
        {
            throw new Exception(message + " (" + line + ":" + column + ")");
        }
    }
}
